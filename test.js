const puppeteer = require("puppeteer");
const expect = require("expect-puppeteer");
const path = require("path");
(async () => {
  const browser = await puppeteer.launch({
    headless: true,
    args: [
      "--disable-gpu",
      "--disable-dev-shm-usage",
      "--disable-setuid-sandbox",
      "--no-sandbox",
    ],
  });
  const page = await browser.newPage();
  await page.goto(`file:${path.join(__dirname, 'index.html')}`)
  await expect(page).toMatchElement("h1", { text: "Cambio attuale" });

  await browser.close();
})()
  .then(() => {console.log("done");process.exit(0)})
  .catch((e) => {console.error(e);process.exit(-1)});
